package com.app.compare3;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.app.compare.FileCompareStrategy;
import com.app.compare.FileCompareStrategyByFileContent;

import lombok.extern.log4j.Log4j;

/**
 * <pre>
 * 移动总部二期：
 * 对比DEV/RM分支，原整工程和拆分后的三个工程代码是否一致，对比结果：
 * 文件/目录  是否差异		差异值
 * XXX.JAVA  有               原有，拆无
 * XXX.JAVA  有               原无，拆有
 * XXX.JAVA  有               原和拆内容不一致
 * 
 * </pre>
 * 
 * @author wangtlc
 * @date 2016年3月7日 下午2:30:50
 *
 *       修改日期 修改人 修改目的
 *
 */
@Log4j
public class App extends BaseApp {
	//白名单
	private static String APP_CONF = null;

	public App(FileCompareStrategy fileCompareStrategy, String[] dirsOlds, String[] dirsNews) {
		super(fileCompareStrategy, dirsOlds, dirsNews);
	}

	private static String[] dirStrOlds;
	private static String[] dirStrNews;

	private List<File> fileListOld = new ArrayList<File>();
	private List<File> fileListNew = new ArrayList<File>();

	// 分析结果
	private List<String> list1 = new ArrayList<String>();// dev1rm0
	private Set<String> set2 = new HashSet<String>();// devrm_no
	private Set<String> set4Code = new HashSet<String>();// ecode
	private List<String> list3 = new ArrayList<String>();// devold 0 devnew 1

	// 处理结果：白名单，即保留处理
	private List<String> list1Blank = new ArrayList<String>();// dev1rm0
	private List<String> list2Blank = new ArrayList<String>();// devrm_no
	private List<String> list3Blank = new ArrayList<String>();// dev0rm1

	public static void main(String[] args) throws Exception {

//		initDevAndRM4Total();
//		 initDevAndRM4Spilt();// RM分支的拆工程前后分析处理
//		 initDev();// DEV分支的拆工程前后分析处理
		// initRM();// RM分支的拆工程前后分析处理

		App app = new App(new FileCompareStrategyByFileContent(), dirStrOlds, dirStrNews);
		app.deal();
	}

	/**
	 * <pre>
	 * 对比两个SVN：拆分前的工程
	 * 输出：new 为 rm	old 为dev
	 * </pre>
	 * 
	 * @author wangtlc
	 * @date 2016年3月10日 上午11:31:14
	 *
	 */
	private static void initDevAndRM4Total() {
		APP_CONF="app.conf";
		dirStrOlds = new String[] { "E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt/src/main/" };
		dirStrNews = new String[] { "E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt/src/main/" };
	}

	// 对比两个SVN：拆分后的三个工程
	private static void initDevAndRM4Spilt() {
		APP_CONF="app.conf";
		dirStrOlds = new String[] { "E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/base-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/cust-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/pay-module/src/main/" };
		dirStrNews = new String[] { "E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/base-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/cust-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/pay-module/src/main/" };
	}

	// 对比拆分前、后的工程
	private static void initRM() {
		APP_CONF="app2.conf";
		dirStrOlds = new String[] { "E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt/src/main/" };
		dirStrNews = new String[] { "E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/base-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/cust-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/echd-chinamobile-jt-prod/web-jt-parent/pay-module/src/main/" };
	}

	// 对比拆分前、后的工程
	private static void initDev() {
		APP_CONF="app2.conf";
		dirStrOlds = new String[] { "E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt/src/main/" };
		dirStrNews = new String[] { "E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/base-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/cust-module/src/main/",
				"E:/workspace-stq-all/chinamobile-jt/chinamobile-jt-dev/web-jt-parent/pay-module/src/main/" };
	}

	private void deal() throws Exception {
		initBlank();
		log.info("初始化完成,开始处理");
		iniFileListInDirs(fileListOld, dirStrOlds);
		iniFileListInDirs(fileListNew, dirStrNews);
		log.info("devFileListOld数为：" + fileListOld.size() + ", devFileListNew数为：" + fileListNew.size());

		dealExistFileList(fileListOld, dirStrOlds, dirStrNews, list1);
		dealExistFileList(fileListNew, dirStrNews, dirStrOlds, list3);

		dealEqualsFileList(fileListOld, dirStrOlds, dirStrNews, set2, set4Code);
		dealEqualsFileList(fileListNew, dirStrNews, dirStrOlds, set2, set4Code);

		log.info("处理完毕，结果如下：");
		printResult("new-notexsit,", list1, list1Blank);
		printResult("old_new_notequal,", set2, list2Blank);
		printResult("old-notexsit,", list3, list3Blank);
		printResult("encode,", set4Code, null);
		log.info("输出完毕！");
	}

	// 初始化白名单
	private void initBlank() throws Exception {
		List<String> lines = IOUtils.readLines(App.class.getResourceAsStream(APP_CONF), "UTF-8");
		String lockOwner = "";
		for (String line : lines) {
			if (line.startsWith("#")) {// 注释
				if (line.contains("new-notexsit-blank")) {
					lockOwner = "new-notexsit-blank";
				} else if (line.contains("old_new_notequal-blank")) {
					lockOwner = "old_new_notequal-blank";
				} else if (line.contains("old-notexsit-blank")) {
					lockOwner = "old-notexsit-blank";
				}
			} else if (StringUtils.isNotBlank(line)) {
				if (lockOwner.equals("new-notexsit-blank")) {
					list1Blank.add(line);
				} else if (lockOwner.equals("old_new_notequal-blank")) {
					list2Blank.add(line);
				} else if (lockOwner.equals("old-notexsit-blank")) {
					list3Blank.add(line);
				} else {
					log.warn("注意，出现未知错误！！");
				}
			}
		}
	}
}
